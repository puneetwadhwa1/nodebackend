#FROM ubuntu:latest

# Getting Base Image\
FROM node:16

MAINTAINER puneet wadhwa <puneetwadhwa@fynd.com>


# Change working directory
WORKDIR /usr/src/app

COPY package*.json ./

# Copy source code
COPY . .



# Install dependencies
RUN npm install

#FROM keymetrics/pm2:latest-alpine

# Expose API port to the outside
EXPOSE 8080

CMD [ "node", "server.js" ]

# # Launch application
#CMD ["npm","start"]
